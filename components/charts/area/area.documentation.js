import * as description from './area.md';
import examples from './examples';

export default {
  followsDesignSystem: true,
  description,
  examples,
};
