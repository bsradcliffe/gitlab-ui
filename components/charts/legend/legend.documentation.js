import * as description from './legend.md';
import examples from './examples';

export default {
  description,
  examples,
};
