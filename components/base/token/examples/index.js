import TokenExample from './token.basic.example.vue';

export default [
  {
    name: 'Basic',
    items: [
      {
        id: 'token-basic',
        name: 'Basic',
        description: 'Basic Token',
        component: TokenExample,
      },
    ],
  },
];
