import LabelBasicExample from './label.basic.example.vue';

export default [
  {
    name: 'Basic',
    items: [
      {
        id: 'label-basic',
        name: 'default',
        description: 'Basic Label',
        component: LabelBasicExample,
      },
    ],
  },
];
