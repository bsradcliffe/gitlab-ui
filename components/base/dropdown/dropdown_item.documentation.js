import * as description from './dropdown_item.md';

export default {
  description,
  bootstrapComponent: 'b-dropdown-item',
};
