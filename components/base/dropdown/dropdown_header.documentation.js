import * as description from './dropdown_header.md';

export default {
  description,
  bootstrapComponent: 'b-dropdown-header',
};
