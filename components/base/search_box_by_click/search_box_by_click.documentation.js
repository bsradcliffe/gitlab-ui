import * as description from './search_box_by_click.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-form-input',
};
