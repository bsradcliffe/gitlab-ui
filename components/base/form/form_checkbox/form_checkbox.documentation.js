import * as description from './form_checkbox.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-form-checkbox',
  followsDesignSystem: true,
};
