import * as description from './form_textarea.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-form-textarea',
};
